Licenses for Old Racetrack
--------------------------
--------------------------

Textures and modified models from STK media repo
------------------------------------------------
stk_generic_borderDarkGreenA.png
chess.png
racetrack_red.png
racetrack_stadium.png
racetrack_tire.png
stk_generic_sand_a.png
stktex_generic_concreteA.png
stktex_generig_marbleA.png
Logo.png (used in Start.png)
tire.blend (tire3)
stklib_metalSupportBeam_a.blend (stklib_metalSupportBeam_a_main)

Textures and picture files made by myself under CC BY-SA 3+
-----------------------------------------------------------
Skizze.png
Start.png
Screenshot.png

Original track author (published under GPL 3+)
-------------------------------------------------
Ingo Ruhnke

Modifications under CC BY-SA 3+
(by Typhon306)
-------------------------------
Stadium, Tires, Start gate
